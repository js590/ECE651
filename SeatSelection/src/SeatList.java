import seats.*;
import java.util.List;
import java.util.ArrayList;

public class SeatList {
	public List<Seat> allseats = new ArrayList<Seat>();
	
	public void setAllSeats(){
		Seat a = new Aclass("a");
		Seat b = new Aclass("b");
		Seat c = new Aclass("c");
		Seat d = new Aclass("d");
		Seat e = new Aclass("e");
		
		Seat f = new Bclass("f");
		Seat g = new Bclass("g");
		Seat h = new Bclass("h");
		Seat i = new Bclass("i");
		Seat j = new Bclass("j");
		
		Seat k = new Cclass("k");
		Seat l = new Cclass("l");
		Seat m = new Cclass("m");
		Seat n = new Cclass("n");
		Seat o = new Cclass("o");
		
		allseats.add(a);
		allseats.add(b);
		allseats.add(c);
		allseats.add(d);
		allseats.add(e);
		allseats.add(f);
		allseats.add(g);
		allseats.add(h);
		allseats.add(i);
		allseats.add(j);
		allseats.add(k);
		allseats.add(l);
		allseats.add(m);
		allseats.add(n);
		allseats.add(o);
		
	}
}