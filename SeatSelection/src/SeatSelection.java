import seats.*;

import java.util.*;

public class SeatSelection {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try{
			//Initialize all seats in the system
			SeatList sys = new SeatList();
			sys.setAllSeats();
			History.SeatHistory();
			int total = sys.allseats.size();
			//Collection of all available seats
			Seat[] availableSeats = new Seat[total];
			int cnt = 0;
			for(int i = 0; i < total; i++){
				if(!History.hist.containsKey(sys.allseats.get(i).getName())){
					availableSeats[cnt++] = sys.allseats.get(i);
				}
			}
			System.out.println("There are " + availableSeats.length + " seats in the system, available seats are as follows:");
			for(int i = 0; i < cnt; i++){
				System.out.print(availableSeats[i].getName() + ", ");
			}
			//user selection
			String usrin = "";
			Scanner sc = new Scanner(System.in);
			System.out.println("Please enter your name:");
			String usrname = sc.nextLine();
			while(true){
				System.out.println("Please select the seat you would like:");
				usrin = sc.nextLine();
				if(usrin.charAt(0) > 'o'){
					System.out.println("Invalid input! Please select again.");
					continue;
				}
				System.out.println("Your selection is \"" + usrin + "\". Please enter Y to confirm or N to reselect:");
				String cc = sc.nextLine();
				if(cc.equals("Y")){
					sc.close();
					break;
				}
			}
			char selected = new Character(usrin.charAt(0));
			Seat se;
			if(selected >= 'a' && selected <= 'e'){
				se = new Aclass(usrin);
			}
			else if(selected >= 'f' && selected <= 'j'){
				se = new Bclass(usrin);
				
			}
			else{
				se = new Cclass(usrin);
			}
			
			//Return price and charge for the money
			System.out.println("The price for this seat is " + se.price());
			if(selected >= 'f' && selected <= 'j'){
				Bclass sese = new Bclass(usrin);
				sese.free();
			}
			
			//Record the usr and selection in system
			History.hist.put(se.getName(), usrname);
		}catch(Exception e){
			System.out.println("Something goes wrong. Please shut down and restart our system.");
			e.printStackTrace();
		}finally{
			System.out.println("System user record:");
			Iterator<String> ite = History.hist.keySet().iterator();
			while(ite.hasNext()){
				String tmp = ite.next();
				System.out.println("Seat: " + tmp + ", Customer: " + History.hist.get(tmp));
			}
			System.out.println("System shutting down...");
		}
		
	}

}
