package seats;

public abstract class Seat {
	private String name;
	
	public Seat(String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void rename(String name){
		this.name = name;
	}
	
	public abstract int price();
}
