package seats;

public class Bclass extends Seat implements Price {

	public Bclass(String name) {
		super(name);
	}

	@Override
	public int price() {
		return 50;
	}
	
	@Override
	public void free(){
		System.out.println("Congraduations! You can have the seat for free!");
	}

}
