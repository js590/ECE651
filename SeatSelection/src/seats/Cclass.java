package seats;

public class Cclass extends Seat {

	public Cclass(String name) {
		super(name);
	}

	@Override
	public int price() {
		return 20;
	}

}
