package seats;

public class Aclass extends Seat {
	
	public Aclass(String name){
		super(name);
	}
	
	@Override
	public int price() {
		return 100;
	}

}
